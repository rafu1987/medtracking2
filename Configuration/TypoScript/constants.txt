plugin.tx_medtracking2 {
	view {
		# cat=plugin.tx_medtracking2/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:medtracking2/Resources/Private/Templates/
		# cat=plugin.tx_medtracking2/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:medtracking2/Resources/Private/Partials/
		# cat=plugin.tx_medtracking2/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:medtracking2/Resources/Private/Layouts/
	}
	persistence {
		# cat=plugin.tx_medtracking2//a; type=string; label=Default storage PID
		storagePid =
	}
}