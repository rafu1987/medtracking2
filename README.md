medtracking2
============

A TYPO3 extension, which includes Google Analytics event tracking and intelliAd tracking. You can either define fixed settings or include the plugin on a powermail "Thank you" page to fill the data, with the data, the user entered in the powermail form. Works with old (< 2.0) and new (> 2.0) powermail versions.

Powered by medialis.net UG (haftungsbeschränkt)
