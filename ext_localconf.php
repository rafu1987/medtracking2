<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'MED.' . $_EXTKEY,
	'Tracking',
	array(
		'Tracking' => 'list',
		
	),
	// non-cacheable actions
	array(
		'Tracking' => '',
		
	)
);
