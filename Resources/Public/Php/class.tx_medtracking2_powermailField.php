<?php

/* * *************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Raphael Zschorsch <zschorsch@medialis.net>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 * ************************************************************* */

class tx_medtracking2_powermailField {

    public function fieldSelect($params) {
        global $TCA, $TYPO3_DB, $LANG;

        $items = $params['items'];
        $config = $params['config'];

        // Get flexform field "field_link"
        $flexFormContent = t3lib_div::xml2array($params['row']['pi_flexform']);
        
        $powermailPage = $flexFormContent['data']['sDEF']['lDEF']['settings.powermailPage']['vDEF'];      

        // Get lang of the tt_content record
        $lang = $params['row']['sys_language_uid'];
        // Set lang to 0 if tt_content record is set for all languages
        if ($lang == -1)
            $lang = 0;
        
        $powermailVersion = t3lib_extMgm::getExtensionVersion("powermail");  
        
        // Old powermail version below 2.0.0
        if(version_compare($powermailVersion,"2.0.0","<")) {        
            // Get powermail fields    
            $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery('uid,title', 'tx_powermail_fields', 'hidden=0 AND deleted=0 AND pid=' . $powermailPage . ' AND sys_language_uid=' . $lang, '', 'sorting', '');
            $num_rows = $GLOBALS['TYPO3_DB']->sql_num_rows($res);

            // Display fields if there is a contact form on the selected page
            if ($num_rows > 0) {
                while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
                    $params['items'][$row['uid']] = array($row['title'], $row['uid']);
                }
            }
            // If no contact form is on selected page, show nothing
            else {
                $params['items']['0'] = array("", "");
            }
        }
        
        // New powermail version above 2.0.0
        else { 
            // Get powermail fields                
            $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery('uid,title', 'tx_powermail_domain_model_fields', 'hidden=0 AND deleted=0 AND pid=' . $powermailPage . ' AND sys_language_uid=' . $lang, '', 'sorting', '');
            $num_rows = $GLOBALS['TYPO3_DB']->sql_num_rows($res);
            
            // Display fields if there is a contact form on the selected page
            if ($num_rows > 0) {
                while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
                    $params['items'][$row['uid']] = array($row['title'], $row['uid']);
                }
            }
            // If no contact form is on selected page, show nothing
            else {
                $params['items']['0'] = array("", "");
            }            
        }
    }
    
    protected function debug($var) {
        t3lib_utility_Debug::debug($var);
    }

}

?>