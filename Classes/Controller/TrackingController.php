<?php
namespace MED\Medtracking2\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Raphael Zschorsch <zschorsch@medialis.net>, medialis.net UG (haftungsbeschränkt)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * TrackingController
 */
class TrackingController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * trackingRepository
	 *
	 * @var \MED\Medtracking2\Domain\Repository\TrackingRepository
	 * @inject
	 */
	protected $trackingRepository = NULL;

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$tracking = $this->trackingRepository->findAll();

        // Order ID
        $orderID = $this->settings['orderID'];
        if(!$orderID) {
            $powermailMailUid = $GLOBALS['TSFE']->fe_user->sesData['medtracking2Uid'];
            $orderID = $powermailMailUid;
        }

		// intelliAd + Google Analytics
		if($this->settings['conversionTracking'] == 1) {
			$intelliAd = true;
			$google = false;
		}
		else if($this->settings['conversionTracking'] == 2) {
			$intelliAd = false;
			$google = true;
		}
		else {
			$intelliAd = true;
			$google = true;
		}

		// Render template
		$view = new \TYPO3\CMS\Fluid\View\StandaloneView();
		$extbaseFrameworkConfiguration = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
		$templateRootPath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($extbaseFrameworkConfiguration['view']['templateRootPath']);
		$templatePathAndFilename = $templateRootPath . 'Tracking/Tracking.html';

		$view->setTemplatePathAndFilename($templatePathAndFilename);

		$view->assign('orderID', $orderID);
		$view->assign('settings', $this->settings);
		$view->assign('confValue', (float) $this->settings['confValue']);
		$view->assign('confValueGoogle', round($this->settings['confValue']));

		// Values
		$view->assign('customValue1', $this->customValue(1, $this->settings['customValue1Select']));
		$view->assign('customValue2', $this->customValue(2, $this->settings['customValue2Select']));
		$view->assign('customValue3', $this->customValue(3, $this->settings['customValue3Select']));
		$view->assign('customValue4', $this->customValue(4, $this->settings['customValue4Select']));
		$view->assign('customValue5', $this->customValue(5, $this->settings['customValue5Select']));

		$view->assign('intelliAd', $intelliAd);
		$view->assign('google', $google);
		
		$content = $view->render();

		// Render JS
		$GLOBALS['TSFE']->additionalFooterData["medtracking2"] = $content;
	}

	public function customValue($value, $type) {
        if ($type == 'input')
            $customValue = $this->settings['customValue'.$value];
        else {
            $customValue = $GLOBALS['TSFE']->fe_user->sesData['medtracking2']['tx_powermail_pi1']['field'][$this->settings['customValue'.$value.'Powermail']];
            
            // If powermail field is an array
            if(is_array($customValue)) {
                foreach($customValue as $cKey => $cVal) {
                    if(empty($cVal)) unset($customValue[$cKey]);
                }
                
                $customValue = implode(",",$customValue);
            }
        }

        return $customValue;
	}

}