<?php

class Tx_Medtracking2_Controller_FormsController extends Tx_Powermail_Controller_FormsController {

    protected function redirectToTarget() {
        $GLOBALS['TSFE']->fe_user->setKey('ses', 'medtracking2', $_POST);
        
        /* powermail 2.0.6 bietet zur Zeit (21.03.2013) keine Möglichkeit, es so zu hooken, dass man die uid des
         * Datenbank-Eintrages irgendwie abfangen kann, und in der Session speichern kann. Deswegen hier ein Select 
         * auf den letzten Datenbank-Eintrag anhand der uid. (Kann zu Problemen führen, wenn zwei Personen gleichzeitig 
         * das Formular ausfüllen und absenden. (Tobias Schenk, Raphael Zschorsch, 21.03.2013)
         */
        
        // Get DB entry
        $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('uid', 'tx_powermail_domain_model_mails', '', $groupBy= '', $orderBy= 'uid DESC');
        $GLOBALS['TSFE']->fe_user->setKey('ses', 'medtracking2Uid', $row['uid']);

        parent::redirectToTarget();
    }

}

?>